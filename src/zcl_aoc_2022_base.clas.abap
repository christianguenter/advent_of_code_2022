CLASS zcl_aoc_2022_base DEFINITION
  PUBLIC
  ABSTRACT
  CREATE PUBLIC .

  PUBLIC SECTION.
    INTERFACES:
      if_oo_adt_classrun ALL METHODS ABSTRACT.

    CONSTANTS:
      BEGIN OF c_puzzle,
        part_1 TYPE i VALUE 1,
        part_2 TYPE i VALUE 2,
        test   TYPE i VALUE 3,
      END OF c_puzzle.

    METHODS:
      constructor
        IMPORTING
          day TYPE i,

      solve ABSTRACT
        IMPORTING
          part          TYPE i
        RETURNING
          VALUE(result) TYPE string,

      solve2 ABSTRACT
        IMPORTING
          part          TYPE i
        RETURNING
          VALUE(result) TYPE string.

  PROTECTED SECTION.
    TYPES:
      t_char   TYPE c LENGTH  1,
      tt_chars TYPE STANDARD TABLE OF t_char
               WITH NON-UNIQUE DEFAULT KEY.

    DATA:
      out TYPE REF TO if_oo_adt_intrnl_classrun.

    METHODS:
      split_string_to_lines
        IMPORTING
          input         TYPE string
        RETURNING
          VALUE(result) TYPE stringtab,

      get_puzzle_input
        IMPORTING
          part          TYPE i
        RETURNING
          VALUE(result) TYPE string,

      split_string_to_chars
        IMPORTING
          i_string      TYPE string
        RETURNING
          VALUE(result) TYPE tt_chars,

      reverse_table
        IMPORTING
          input  TYPE INDEX TABLE
        EXPORTING
          result TYPE INDEX TABLE.

  PRIVATE SECTION.
    CONSTANTS:
      year TYPE gjahr VALUE '2022'.

    DATA:
      day  TYPE i.

ENDCLASS.



CLASS zcl_aoc_2022_base IMPLEMENTATION.

  METHOD split_string_to_chars.

    DO strlen( i_string ) TIMES.

      INSERT CONV #( substring( val = i_string off = sy-index - 1 len = 1 ) )
        INTO TABLE result.

    ENDDO.

  ENDMETHOD.

  METHOD constructor.

    me->day  = day.

  ENDMETHOD.


  METHOD split_string_to_lines.

    SPLIT input AT cl_abap_char_utilities=>cr_lf INTO TABLE result.

  ENDMETHOD.


  METHOD get_puzzle_input.

    result = zcl_advent_of_code=>get_puzzle_input(
                year = year
                user = sy-uname
                day  = day
                part = part ).

  ENDMETHOD.


  METHOD reverse_table.

    LOOP AT input ASSIGNING FIELD-SYMBOL(<line>).
      INSERT <line> INTO result INDEX 1.
    ENDLOOP.

  ENDMETHOD.

ENDCLASS.
