*"* use this source file for your ABAP unit test classes
CLASS ltcl_test DEFINITION FINAL FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS.

  PRIVATE SECTION.
    DATA:
      cut TYPE REF TO zcl_aoc_2022_02.

    METHODS:
      setup,

      solve FOR TESTING RAISING cx_static_check.

ENDCLASS.


CLASS ltcl_test IMPLEMENTATION.

  METHOD setup.

    cut = NEW zcl_aoc_2022_02( ).

  ENDMETHOD.

  METHOD solve.

    cl_abap_unit_assert=>assert_equals(
        act = cut->solve( zcl_aoc_2022_base=>c_puzzle-test )
        exp = 15 ).
    cl_abap_unit_assert=>assert_equals(
        act = cut->solve2( zcl_aoc_2022_base=>c_puzzle-test )
        exp = 12 ).

    cl_abap_unit_assert=>assert_equals(
        act = cut->solve( zcl_aoc_2022_base=>c_puzzle-part_1 )
        exp = 10404 ).
    cl_abap_unit_assert=>assert_equals(
        act = cut->solve2( zcl_aoc_2022_base=>c_puzzle-part_2 )
        exp = 10334 ).

  ENDMETHOD.

ENDCLASS.
