CLASS zcl_aoc_2022_02 DEFINITION
  PUBLIC
  INHERITING FROM zcl_aoc_2022_base
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      constructor,

      if_oo_adt_classrun~main REDEFINITION,
      solve REDEFINITION,
      solve2 REDEFINITION.

  PRIVATE SECTION.
    TYPES:
      BEGIN OF t_shape,
        points TYPE i,
      END OF t_shape.

    METHODS:
      eval
        IMPORTING
          p1_shape      TYPE zcl_aoc_2022_02=>t_shape
          p2_shape      TYPE zcl_aoc_2022_02=>t_shape
        RETURNING
          VALUE(result) TYPE i.

    DATA:
      rock     TYPE t_shape,
      paper    TYPE t_shape,
      scissors TYPE t_shape.

ENDCLASS.



CLASS zcl_aoc_2022_02 IMPLEMENTATION.

  METHOD constructor.

    super->constructor( day = 2 ).

    rock     = VALUE t_shape( points = 1 ).
    paper    = VALUE t_shape( points = 2 ).
    scissors = VALUE t_shape( points = 3 ).

  ENDMETHOD.


  METHOD eval.

    result = p2_shape-points
           + COND #(

               " draw
               WHEN p1_shape = p2_shape
                 THEN 3

               " win
               WHEN p1_shape = rock     AND p2_shape = paper
                 OR p1_shape = paper    AND p2_shape = scissors
                 OR p1_shape = scissors AND p2_shape = rock
                 THEN 6

               " lose
               ELSE 0 ).

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.

    out->write( solve( c_puzzle-test ) ).
    out->write( solve( c_puzzle-part_1 ) ).
    out->write( solve2( c_puzzle-test ) ).
    out->write( solve2( c_puzzle-part_2 ) ).

  ENDMETHOD.


  METHOD solve.

    DATA(input) = get_puzzle_input( part ).

    LOOP AT split_string_to_lines( input ) ASSIGNING FIELD-SYMBOL(<line>).

      SPLIT <line> AT space INTO DATA(p1) DATA(p2).

      DATA(p1_shape) = COND #(
                         WHEN p1 = 'A' THEN rock
                         WHEN p1 = 'B' THEN paper
                         WHEN p1 = 'C' THEN scissors ).

      DATA(p2_shape) = COND #(
                         WHEN p2 = 'X' THEN rock
                         WHEN p2 = 'Y' THEN paper
                         WHEN p2 = 'Z' THEN scissors ).

      result = result
             + eval(
                 p1_shape = p1_shape
                 p2_shape = p2_shape ).

    ENDLOOP.

  ENDMETHOD.


  METHOD solve2.

    DATA(input) = get_puzzle_input( part ).

    LOOP AT split_string_to_lines( input ) ASSIGNING FIELD-SYMBOL(<line>).

      SPLIT <line> AT space INTO DATA(p1) DATA(p2).

      DATA(p1_shape) = COND #(
                         WHEN p1 = 'A' THEN rock
                         WHEN p1 = 'B' THEN paper
                         WHEN p1 = 'C' THEN scissors ).

      DATA(p2_shape) = COND t_shape(
                         " lose
                         WHEN p2 = 'X' THEN COND t_shape(
                                                WHEN p1_shape = rock		 THEN scissors
                                                WHEN p1_shape = paper 	 THEN rock
                                                WHEN p1_shape = scissors THEN paper )

                         " draw
                         WHEN p2 = 'Y' THEN p1_shape

                         " win
                         WHEN p2 = 'Z' THEN COND t_shape(
                                                WHEN p1_shape = rock		  THEN paper
                                                WHEN p1_shape = paper 	  THEN scissors
                                                WHEN p1_shape = scissors  THEN rock ) ).

      result = result
             + eval(
                 p1_shape = p1_shape
                 p2_shape = p2_shape ).

    ENDLOOP.

  ENDMETHOD.

ENDCLASS.
