*"* use this source file for any type of declarations (class
*"* definitions, interfaces or type declarations) you need for
*"* components in the private section

INTERFACE if_node DEFERRED.

INTERFACE if_callback.
  METHODS:
    process
      IMPORTING node TYPE REF TO if_node.
ENDINTERFACE.

INTERFACE if_node.

  DATA:
    parent   TYPE REF TO if_node,
    children TYPE STANDARD TABLE OF REF TO if_node,
    size     TYPE i,
    name     TYPE string,
    is_dir   TYPE abap_bool.

  METHODS:
    add_child
      IMPORTING
        child TYPE REF TO if_node,

    get_child
      IMPORTING
        name          TYPE string
        is_dir        TYPE abap_bool
      RETURNING
        VALUE(result) TYPE REF TO if_node,

    get_total_size
      RETURNING
        VALUE(result) TYPE i,

    print
      IMPORTING
        i_depth       TYPE i OPTIONAL
      RETURNING
        VALUE(result) TYPE string,

    walk_dir
      IMPORTING
        callback TYPE REF TO if_callback.

ENDINTERFACE.


CLASS lcl_node DEFINITION.

  PUBLIC SECTION.
    INTERFACES:
      if_node.

    ALIASES:
      parent   FOR if_node~parent,
      children FOR if_node~children,
      size     FOR if_node~size,
      name     FOR if_node~name,
      is_dir   FOR if_node~is_dir.

    METHODS:
      constructor
        IMPORTING
          parent TYPE REF TO if_node OPTIONAL
          size   TYPE i
          name   TYPE string
          is_dir TYPE abap_bool.

ENDCLASS.

CLASS lcl_collector DEFINITION.

  PUBLIC SECTION.
    INTERFACES:
      if_callback.

    METHODS:
      get_sum RETURNING VALUE(result) TYPE i.

  PRIVATE SECTION.
    DATA:
      sum TYPE i.

ENDCLASS.


CLASS lcl_free_space_collector DEFINITION.

  PUBLIC SECTION.
    INTERFACES:
      if_callback.

    METHODS:
      constructor
        IMPORTING
          required_space TYPE i,

      get_smallest_dir
        RETURNING
          VALUE(result) TYPE i.

  PRIVATE SECTION.
    DATA:
      required_space TYPE i,
      smallest_dir   TYPE i.

ENDCLASS.
