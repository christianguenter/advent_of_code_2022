*"* use this source file for your ABAP unit test classes
CLASS ltcl_test DEFINITION FINAL FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS.

  PRIVATE SECTION.
    DATA:
      cut TYPE REF TO zcl_aoc_2022_07.

    METHODS:
      setup,
      solve FOR TESTING RAISING cx_static_check,
      solve2 FOR TESTING RAISING cx_static_check.

ENDCLASS.


CLASS ltcl_test IMPLEMENTATION.

  METHOD setup.

    cut = NEW zcl_aoc_2022_07( ).

  ENDMETHOD.

  METHOD solve.

    cl_abap_unit_assert=>assert_equals(
        act = cut->solve( cut->c_puzzle-test )
        exp = 95437 ).

    cl_abap_unit_assert=>assert_equals(
        act = cut->solve( cut->c_puzzle-part_1 )
        exp = 1432936 ).

  ENDMETHOD.


  METHOD solve2.

    cl_abap_unit_assert=>assert_equals(
        act = cut->solve2( cut->c_puzzle-test )
        exp = 24933642 ).

    cl_abap_unit_assert=>assert_equals(
        act = cut->solve2( cut->c_puzzle-part_2 )
        exp = 272298 ).

  ENDMETHOD.

ENDCLASS.
