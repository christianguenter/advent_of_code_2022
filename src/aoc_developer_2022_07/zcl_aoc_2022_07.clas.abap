CLASS zcl_aoc_2022_07 DEFINITION
  PUBLIC
  INHERITING FROM zcl_aoc_2022_base
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      constructor,
      solve REDEFINITION,
      solve2 REDEFINITION,
      if_oo_adt_classrun~main REDEFINITION.

  PRIVATE SECTION.
    TYPES:
      BEGIN OF t_term_input,
        id        TYPE string,
        type      TYPE c LENGTH 1,
        arguments TYPE stringtab,
      END OF t_term_input,
      tt_term_input TYPE STANDARD TABLE OF t_term_input
                    WITH NON-UNIQUE DEFAULT KEY.

    CONSTANTS:
      BEGIN OF c_types,
        command TYPE c LENGTH 1 VALUE 'C',
        output  TYPE c LENGTH 1 VALUE 'O',
      END OF c_types,

      BEGIN OF c_ids,
        change_dir TYPE string VALUE `CHANGE_DIR`,
        list       TYPE string VALUE `LIST`,
        dir        TYPE string VALUE `DIR`,
        file       TYPE string VALUE `FILE`,
      END OF c_ids.

    METHODS:
      parse
        IMPORTING
          line          TYPE string
        RETURNING
          VALUE(result) TYPE t_term_input,

      build_tree
        IMPORTING
          term_inputs   TYPE zcl_aoc_2022_07=>tt_term_input
        RETURNING
          VALUE(result) TYPE REF TO if_node.

ENDCLASS.



CLASS zcl_aoc_2022_07 IMPLEMENTATION.


  METHOD build_tree.

    DATA:
      node        TYPE REF TO if_node,
      current_dir TYPE REF TO if_node.

    LOOP AT term_inputs INTO DATA(term_input).

      CASE term_input-id.
        WHEN c_ids-change_dir.

          DATA(dir) = term_input-arguments[ 1 ].

          IF dir = '/'.
            result = NEW lcl_node(
              size   = 0
              name   = dir
              is_dir = abap_true ).
            current_dir = result.
          ELSEIF dir = '..'.
            current_dir = current_dir->parent.
          ELSE.

            node = current_dir->get_child(
                     name   =  dir
                     is_dir = abap_true ).

            IF node IS INITIAL.
              node = NEW lcl_node(
                parent = current_dir
                size   = 0
                name   = dir
                is_dir = abap_true ).
              current_dir->add_child( node ).
            ENDIF.

            current_dir = node.
          ENDIF.

        WHEN c_ids-list.

        WHEN c_ids-dir.

          dir = term_input-arguments[ 1 ].

          node = NEW lcl_node(
            parent = current_dir
            size   = 0
            name   = dir
            is_dir = abap_true ).
          current_dir->add_child( node ).

        WHEN c_ids-file.

          DATA(size) = term_input-arguments[ 1 ].
          DATA(filename) = term_input-arguments[ 2 ].

          node = NEW lcl_node(
            parent = current_dir
            size   = CONV #( size )
            name   = filename
            is_dir = abap_false ).
          current_dir->add_child( node ).

      ENDCASE.

    ENDLOOP.

  ENDMETHOD.


  METHOD constructor.

    super->constructor( day = 7 ).

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.

    me->out = out.

    ASSERT solve( c_puzzle-test ) = 95437.
    ASSERT solve( c_puzzle-part_1 ) = 1432936.
    ASSERT solve2( c_puzzle-test ) = 24933642.
    ASSERT solve2( c_puzzle-part_2 ) = 272298.

    out->write( solve2( c_puzzle-part_2 ) ).

  ENDMETHOD.


  METHOD parse.

    TYPES:
      BEGIN OF t_token,
        id    TYPE string,
        regex TYPE string,
        type  TYPE c LENGTH 1,
      END OF t_token,
      tt_tokens TYPE SORTED TABLE OF t_token
                     WITH UNIQUE DEFAULT KEY.

    DATA(tokens) = VALUE tt_tokens(
                     ( id = c_ids-change_dir type = c_types-command regex = '\$\scd\s(.*)' )
                     ( id = c_ids-list       type = c_types-command regex = '\$\sls' )
                     ( id = c_ids-dir        type = c_types-output  regex = 'dir\s(.*)' )
                     ( id = c_ids-file       type = c_types-output  regex = '(\d+)\s(.+)' )
                   ).

    LOOP AT tokens ASSIGNING FIELD-SYMBOL(<t>).

      FIND
        FIRST OCCURRENCE OF REGEX <t>-regex
        IN line
        SUBMATCHES DATA(s1) DATA(s2).
      IF sy-subrc = 0.

        result-id = <t>-id.

        IF s1 IS NOT INITIAL.
          INSERT s1 INTO TABLE result-arguments.
        ENDIF.

        IF s2 IS NOT INITIAL.
          INSERT s2 INTO TABLE result-arguments.
        ENDIF.

      ENDIF.

    ENDLOOP.

  ENDMETHOD.


  METHOD solve.

    DATA(term_input) = VALUE tt_term_input(
                         FOR <line> IN split_string_to_lines( get_puzzle_input( part ) )
                         ( parse( <line> ) ) ).

    DATA(collector) = NEW lcl_collector( ).

    build_tree( term_input )->walk_dir( CAST if_callback( collector ) ).

    result = collector->get_sum( ).

  ENDMETHOD.


  METHOD solve2.

    DATA(term_input) = VALUE tt_term_input(
                         FOR <line> IN split_string_to_lines( get_puzzle_input( part ) )
                         ( parse( <line> ) ) ).

    DATA(root) = build_tree( term_input ).

    DATA(occupied_space) =  root->get_total_size( ).
    DATA(free_space) = 70000000 - occupied_space.
    DATA(required_space) = 30000000 - free_space.

    DATA(collector) = NEW lcl_free_space_collector( required_space ).

    root->walk_dir( CAST if_callback( collector ) ).

    result = collector->get_smallest_dir( ).

  ENDMETHOD.
ENDCLASS.
