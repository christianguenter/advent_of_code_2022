*"* use this source file for the definition and implementation of
*"* local helper classes, interface definitions and type
*"* declarations

CLASS lcl_node IMPLEMENTATION.

  METHOD constructor.

    me->parent = parent.
    me->size = size.
    me->name = name.
    me->is_dir = is_dir.

  ENDMETHOD.

  METHOD if_node~add_child.

    INSERT child INTO TABLE children.

  ENDMETHOD.


  METHOD if_node~print.

    IF is_dir = abap_true.
      DO i_depth TIMES.
        result = result && |\t|.
      ENDDO.
    ENDIF.

    result = |- { name } { COND #(
                             WHEN is_dir = abap_true
                             THEN |(dir, size { if_node~get_total_size( ) })|
                             ELSE |(file, size { size })| )
                          && cl_abap_char_utilities=>cr_lf }|.


    LOOP AT children ASSIGNING FIELD-SYMBOL(<c>).
      IF is_dir = abap_true.
        DO i_depth + 1 TIMES.
          result = result && |\t|.
        ENDDO.
      ENDIF.
      result = result && <c>->print( i_depth + 1 ) && cl_abap_char_utilities=>cr_lf.
    ENDLOOP.

  ENDMETHOD.


  METHOD if_node~get_child.

    LOOP AT children ASSIGNING FIELD-SYMBOL(<c>).

      IF  <c>->name = name
      AND <c>->is_dir = abap_true.
        result = <c>.
        RETURN.
      ENDIF.

    ENDLOOP.

  ENDMETHOD.


  METHOD if_node~get_total_size.

    IF is_dir = abap_false.
      result = size.
      RETURN.
    ELSE.

      LOOP AT children ASSIGNING FIELD-SYMBOL(<c>).
        result = result + <c>->get_total_size( ).
      ENDLOOP.

    ENDIF.

  ENDMETHOD.


  METHOD if_node~walk_dir.

    IF is_dir = abap_true.
      callback->process( me ).
    ENDIF.

    LOOP AT children ASSIGNING FIELD-SYMBOL(<c>).
      <c>->walk_dir( callback ).
    ENDLOOP.

  ENDMETHOD.

ENDCLASS.


CLASS lcl_collector IMPLEMENTATION.

  METHOD if_callback~process.

    DATA(size) = node->get_total_size( ).
    IF size <= 100000.
      sum = sum + size.
    ENDIF.

  ENDMETHOD.

  METHOD get_sum.

    result = sum.

  ENDMETHOD.

ENDCLASS.


CLASS lcl_free_space_collector IMPLEMENTATION.

  METHOD constructor.
    me->required_space = required_space.
  ENDMETHOD.


  METHOD if_callback~process.

    IF node->is_dir = abap_false.
      RETURN.
    ENDIF.

    DATA(size) = node->get_total_size( ).

    IF size > required_space.

      smallest_dir = COND #(
                       WHEN smallest_dir = 0 THEN size
                       ELSE nmin(
                              val1 = smallest_dir
                              val2 = size ) ).

    ENDIF.

  ENDMETHOD.


  METHOD get_smallest_dir.

    result = smallest_dir.

  ENDMETHOD.

ENDCLASS.
