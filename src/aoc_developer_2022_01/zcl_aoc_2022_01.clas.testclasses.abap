CLASS ltcl_test DEFINITION FINAL FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS.

  PRIVATE SECTION.
    DATA:
      cut TYPE REF TO zcl_aoc_2022_01.

    METHODS:
      setup,

      solve FOR TESTING RAISING cx_static_check.

ENDCLASS.


CLASS ltcl_test IMPLEMENTATION.

  METHOD setup.

    cut = NEW zcl_aoc_2022_01( ).

  ENDMETHOD.


  METHOD solve.

    cl_abap_unit_assert=>assert_equals(
        act = cut->solve( zcl_aoc_2022_base=>c_puzzle-test )
        exp = 24000 ).
    cl_abap_unit_assert=>assert_equals(
        act = cut->solve2( zcl_aoc_2022_base=>c_puzzle-test )
        exp = 45000 ).

    cl_abap_unit_assert=>assert_equals(
        act = cut->solve( zcl_aoc_2022_base=>c_puzzle-part_1 )
        exp = 66487 ).
    cl_abap_unit_assert=>assert_equals(
        act = cut->solve2( zcl_aoc_2022_base=>c_puzzle-part_2 )
        exp = 197301 ).

  ENDMETHOD.

ENDCLASS.
