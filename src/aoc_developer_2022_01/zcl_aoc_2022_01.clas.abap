CLASS zcl_aoc_2022_01 DEFINITION
  PUBLIC
  INHERITING FROM zcl_aoc_2022_base
  CREATE PUBLIC.

  PUBLIC SECTION.
    METHODS:
      constructor,
      if_oo_adt_classrun~main REDEFINITION,
      solve REDEFINITION,
      solve2 REDEFINITION.

  PRIVATE SECTION.
    TYPES:
      BEGIN OF t_elve,
        index    TYPE i,
        calories TYPE i,
      END OF t_elve,
      tt_elve TYPE HASHED TABLE OF t_elve
              WITH UNIQUE KEY index
              WITH NON-UNIQUE SORTED KEY key_cal COMPONENTS calories.

    METHODS:
      split
        IMPORTING
          input         TYPE string
        RETURNING
          VALUE(result) TYPE stringtab,

      calculate_elves_calories
        IMPORTING
          input         TYPE string
        RETURNING
          VALUE(result) TYPE zcl_aoc_2022_01=>tt_elve.

ENDCLASS.



CLASS zcl_aoc_2022_01 IMPLEMENTATION.

  METHOD constructor.

    super->constructor( day = 1 ).

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.

    out->write( solve( c_puzzle-part_1 ) ).
    out->write( solve2( c_puzzle-part_2 ) ).

  ENDMETHOD.


  METHOD calculate_elves_calories.

    INSERT
      VALUE #(
        index = 1 )
      INTO TABLE result
      ASSIGNING FIELD-SYMBOL(<elve>).

    LOOP AT split( input ) ASSIGNING FIELD-SYMBOL(<line>).

      IF <line> IS INITIAL.
        INSERT
          VALUE #(
            index = <elve>-index + 1 )
          INTO TABLE result
          ASSIGNING <elve>.
      ENDIF.

      <elve>-calories = <elve>-calories + <line>.

    ENDLOOP.

  ENDMETHOD.


  METHOD solve.

    DATA(elves) = calculate_elves_calories( get_puzzle_input( part ) ).

    result = VALUE #( elves[ KEY key_cal INDEX lines( elves ) ]-calories ).

  ENDMETHOD.


  METHOD solve2.

    DATA(elves) = calculate_elves_calories( get_puzzle_input( part ) ).

    result = VALUE #( elves[ KEY key_cal INDEX lines( elves ) ]-calories )
           + VALUE #( elves[ KEY key_cal INDEX lines( elves ) - 1 ]-calories )
           + VALUE #( elves[ KEY key_cal INDEX lines( elves ) - 2 ]-calories ).

  ENDMETHOD.


  METHOD split.

    SPLIT input AT cl_abap_char_utilities=>cr_lf INTO TABLE result.

  ENDMETHOD.
ENDCLASS.
