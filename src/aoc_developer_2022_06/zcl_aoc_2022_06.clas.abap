CLASS zcl_aoc_2022_06 DEFINITION
  PUBLIC
  INHERITING FROM zcl_aoc_2022_base
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      constructor,
      solve REDEFINITION,
      solve2 REDEFINITION,
      if_oo_adt_classrun~main REDEFINITION.

  PRIVATE SECTION.
    METHODS:
      has_unique_letters
        IMPORTING
          i_candidate   TYPE string
        RETURNING
          VALUE(result) TYPE abap_bool,

      detect_marker
        IMPORTING
          length        TYPE i
          input         TYPE string
        RETURNING
          VALUE(result) TYPE string.

ENDCLASS.



CLASS zcl_aoc_2022_06 IMPLEMENTATION.

  METHOD constructor.

    super->constructor( day = 6 ).

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.

    ASSERT solve( c_puzzle-test ) = 7.
    ASSERT solve( c_puzzle-part_1 ) = 1275.
    ASSERT solve2( c_puzzle-test ) = 19.
    ASSERT solve2( c_puzzle-part_2 ) = 3605.

  ENDMETHOD.



  METHOD solve.

    DATA(input) = get_puzzle_input( part ).

    result = detect_marker(
                length = 4
                input  = input ).

  ENDMETHOD.



  METHOD solve2.

    DATA(input) = get_puzzle_input( part ).

    result = detect_marker(
                length = 14
                input  = input ).

  ENDMETHOD.


  METHOD has_unique_letters.

    DATA: char_count TYPE HASHED TABLE OF c
                          WITH UNIQUE KEY table_line.

    DATA(chars) = split_string_to_chars( i_candidate ).

    LOOP AT chars ASSIGNING FIELD-SYMBOL(<c>).
      INSERT <c> INTO TABLE char_count.
      IF sy-subrc <> 0.
        RETURN.
      ENDIF.
    ENDLOOP.

    result = abap_true.

  ENDMETHOD.


  METHOD detect_marker.

    DO strlen( input ) - length TIMES.

      DATA(candidate) = substring(
                          val = input
                          off = sy-index - 1
                          len = length ).

      IF has_unique_letters( candidate ) = abap_true.
        result = sy-index + length - 1.
        RETURN.
      ENDIF.

    ENDDO.

  ENDMETHOD.

ENDCLASS.
