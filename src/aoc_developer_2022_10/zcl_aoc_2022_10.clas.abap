CLASS zcl_aoc_2022_10 DEFINITION
  PUBLIC
  INHERITING FROM zcl_aoc_2022_base
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      constructor,

      solve REDEFINITION,

      solve2 REDEFINITION,

      if_oo_adt_classrun~main REDEFINITION.

ENDCLASS.



CLASS zcl_aoc_2022_10 IMPLEMENTATION.


  METHOD constructor.

    super->constructor( day = 10 ).

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.

    me->out = out.
    ASSERT solve( c_puzzle-test ) = 13140.
    ASSERT solve( c_puzzle-part_1 ) = 17840.
*    out->write( solve2( c_puzzle-test ) ).
*    out->write( solve2( c_puzzle-part_2 ) ).

  ENDMETHOD.


  METHOD solve.

    DATA(collector) = CAST if_callback( NEW lcl_collector( ) ).

    NEW lcl_cathode_ray(
            i_callback  = collector
            i_input             = split_string_to_lines( get_puzzle_input( part ) )
            i_out               = out
        )->run_cycles( ).

    result = collector->get_result( ).

  ENDMETHOD.


  METHOD solve2.

    DATA(printer) = CAST if_callback( NEW lcl_printer( out ) ).

    NEW lcl_cathode_ray(
            i_callback = printer
            i_input            = split_string_to_lines( get_puzzle_input( part ) )
            i_out              = out
        )->run_cycles( ).

    result = printer->get_result( ).

  ENDMETHOD.

ENDCLASS.
