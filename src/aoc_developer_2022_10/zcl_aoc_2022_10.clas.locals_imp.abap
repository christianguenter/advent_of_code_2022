*"* use this source file for the definition and implementation of
*"* local helper classes, interface definitions and type
*"* declarations


CLASS lcl_collector IMPLEMENTATION.

  METHOD if_callback~callback.

    IF cycle = 20
    OR ( cycle - 20 ) MOD 40 = 0.
      accumulator = accumulator + ( register_x * cycle ).
    ENDIF.

  ENDMETHOD.


  METHOD if_callback~get_result.

    result = accumulator.

  ENDMETHOD.

ENDCLASS.


CLASS lcl_cathode_ray IMPLEMENTATION.

  METHOD constructor.

    before_cycle_ends_callback = i_before_cycle_ends.
    callback = i_callback.
    input = i_input.
    out = i_out.

  ENDMETHOD.


  METHOD run_cycles.

    DATA:
      register_x TYPE i VALUE 1,
      addx_op    TYPE i.

    DATA(cycle) = 1.

    DO.

      IF addx_op IS NOT INITIAL.

        IF callback IS BOUND.
          callback->callback(
              cycle      = cycle
              register_x = register_x ).
        ENDIF.

        register_x = register_x + addx_op.
        CLEAR: addx_op.
        " out->write( |End of cycle: { cycle } X={ register_x } product { cycle * register_x }| ).

      ELSE.

        DATA(next_instruction) = VALUE #( input[ 1 ] OPTIONAL ).

        IF next_instruction IS INITIAL.
          EXIT.
        ENDIF.

        DELETE input INDEX 1.

        FIND
          FIRST OCCURRENCE OF REGEX '([a-z]+)\s?(-?\d+)?'
          IN next_instruction
          SUBMATCHES DATA(command) DATA(parameter).
        ASSERT sy-subrc = 0.

        IF callback IS BOUND.
          callback->callback(
              cycle      = cycle
              register_x = register_x ).
        ENDIF.

        CASE command.
          WHEN 'noop'.


          WHEN 'addx'.

            addx_op = parameter.

        ENDCASE.

        " out->write( |command { command } parameter { parameter }| ).

      ENDIF.

      IF before_cycle_ends_callback IS BOUND.
        before_cycle_ends_callback->callback(
            cycle      = cycle
            register_x = register_x ).
      ENDIF.

      cycle = cycle + 1.

    ENDDO.

  ENDMETHOD.

ENDCLASS.


CLASS lcl_printer IMPLEMENTATION.

  METHOD constructor.

    out = i_out.

  ENDMETHOD.


  METHOD if_callback~callback.

    cursor = cursor + 1.

    " print_sprite( register_x ).

    IF  cursor >= register_x
    AND cursor <= register_x + 2.

      screen_line = screen_line && '#'.

    ELSE.

      screen_line = screen_line && '.'.

    ENDIF.

    out->write( |CRT: { screen_line }| ).

    IF strlen( screen_line ) = 40.
      INSERT screen_line INTO TABLE screen.
      CLEAR:
        screen_line,
        cursor.
    ENDIF.

  ENDMETHOD.


  METHOD if_callback~get_result.

    result = concat_lines_of(
               table = screen
               sep   = cl_abap_char_utilities=>cr_lf ).


  ENDMETHOD.


  METHOD print_sprite.

    out->write( |Sprite Position: { REDUCE string(
                                      INIT result = ||
                                      FOR i = 1 WHILE i < register_x
                                      NEXT result = result && '.' )
                                 && |###| }| ).

  ENDMETHOD.

ENDCLASS.
