*"* use this source file for any type of declarations (class
*"* definitions, interfaces or type declarations) you need for
*"* components in the private section

INTERFACE if_callback.

  METHODS:
    callback
      IMPORTING
        cycle      TYPE i
        register_x TYPE i,

    get_result
      RETURNING
        VALUE(result) TYPE string.

ENDINTERFACE.


CLASS lcl_collector DEFINITION.

  PUBLIC SECTION.
    INTERFACES:
      if_callback.

  PRIVATE SECTION.
    DATA:
      accumulator TYPE i.

ENDCLASS.


CLASS lcl_printer DEFINITION.

  PUBLIC SECTION.
    INTERFACES:
      if_callback.

    METHODS:
      constructor
        IMPORTING
          i_out TYPE REF TO if_oo_adt_intrnl_classrun.

  PRIVATE SECTION.
    DATA:
      cursor      TYPE i,
      screen      TYPE stringtab,
      screen_line TYPE string,
      out         TYPE REF TO if_oo_adt_intrnl_classrun.

    METHODS:
      print_sprite
        IMPORTING
          register_x TYPE i.

ENDCLASS.


CLASS lcl_cathode_ray DEFINITION.

  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING
          i_before_cycle_ends TYPE REF TO if_callback OPTIONAL
          i_callback          TYPE REF TO if_callback OPTIONAL
          i_input             TYPE stringtab
          i_out               TYPE REF TO if_oo_adt_intrnl_classrun,

      run_cycles.

  PRIVATE SECTION.
    DATA:
      input                      TYPE stringtab,
      before_cycle_ends_callback TYPE REF TO if_callback,
      callback                   TYPE REF TO if_callback,
      out                        TYPE REF TO if_oo_adt_intrnl_classrun.

ENDCLASS.
