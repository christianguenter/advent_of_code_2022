*"* use this source file for your ABAP unit test classes
CLASS ltcl_test DEFINITION FINAL FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS.

  PRIVATE SECTION.
    DATA:
      cut TYPE REF TO zcl_aoc_2022_03.

    METHODS:
      setup,
      solve FOR TESTING RAISING cx_static_check,
      solev2 FOR TESTING RAISING cx_static_check.

ENDCLASS.


CLASS ltcl_test IMPLEMENTATION.

  METHOD setup.

    cut = NEW zcl_aoc_2022_03( ).

  ENDMETHOD.


  METHOD solve.

    cl_abap_unit_assert=>assert_equals(
        exp = cut->solve( zcl_aoc_2022_base=>c_puzzle-test )
        act = 157 ).

    cl_abap_unit_assert=>assert_equals(
        exp = cut->solve( zcl_aoc_2022_base=>c_puzzle-part_1 )
        act = 7746 ).

  ENDMETHOD.


  METHOD solev2.

    cl_abap_unit_assert=>assert_equals(
        exp = cut->solve2( zcl_aoc_2022_base=>c_puzzle-test )
        act = 70 ).

    cl_abap_unit_assert=>assert_equals(
        exp = cut->solve2( zcl_aoc_2022_base=>c_puzzle-part_1 )
        act = 2604 ).

  ENDMETHOD.

ENDCLASS.
