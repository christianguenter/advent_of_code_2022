CLASS zcl_aoc_2022_03 DEFINITION
  PUBLIC
  INHERITING FROM zcl_aoc_2022_base
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      solve REDEFINITION,
      solve2 REDEFINITION,
      if_oo_adt_classrun~main REDEFINITION.

    METHODS:
      constructor.

  PRIVATE SECTION.
    DATA:
      priorities TYPE string.

    METHODS:
      find_duplicate_letter
        IMPORTING
          first_compartment  TYPE string
          second_compartment TYPE string
        RETURNING
          VALUE(result)      TYPE t_char,

      prioritize
        IMPORTING
          i_letter      TYPE zcl_aoc_2022_03=>t_char
        RETURNING
          VALUE(result) TYPE i,

      find_badge
        IMPORTING
          i_group_rucksack TYPE stringtab
        RETURNING
          VALUE(result)    TYPE t_char.

ENDCLASS.



CLASS zcl_aoc_2022_03 IMPLEMENTATION.


  METHOD constructor.

    super->constructor( day = '03' ).
    priorities = |{ to_lower( sy-abcde ) }{ sy-abcde }|.

  ENDMETHOD.


  METHOD find_duplicate_letter.

    TYPES:
      tt_unique_letters TYPE HASHED TABLE OF t_char
                             WITH UNIQUE KEY table_line .

    DATA:
      unique_letters TYPE tt_unique_letters.

    LOOP AT split_string_to_chars( first_compartment ) ASSIGNING FIELD-SYMBOL(<letter>).
      INSERT <letter> INTO TABLE unique_letters.
    ENDLOOP.

    LOOP AT split_string_to_chars( second_compartment ) ASSIGNING <letter>.
      IF line_exists( unique_letters[ table_line = <letter> ] ).
        result = <letter>.
        RETURN.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.

    me->out = out.

    ASSERT solve( c_puzzle-test ) = 157.
    ASSERT solve( c_puzzle-part_1 ) = 7746.

    ASSERT solve2( c_puzzle-test ) = 70.
    ASSERT solve2( c_puzzle-part_2 ) = 2604.

  ENDMETHOD.


  METHOD prioritize.

    result = find( val = priorities sub = i_letter ) + 1.

  ENDMETHOD.


  METHOD solve.

    DATA(input) = split_string_to_lines( get_puzzle_input( part ) ).

    LOOP AT input ASSIGNING FIELD-SYMBOL(<line>).

      DATA(length) = strlen( <line> ).

      DATA(first_compartment) = substring(
                                  val = <line>
                                  off = 0
                                  len = length / 2 ).

      DATA(second_compartment) = substring(
                                   val = <line>
                                   off = length / 2
                                   len = length / 2 ).

      DATA(duplicate_letter) = find_duplicate_letter(
                                   first_compartment  = first_compartment
                                   second_compartment = second_compartment ).

      DATA(priority) = prioritize( duplicate_letter ).

      result = result + priority.

    ENDLOOP.

  ENDMETHOD.


  METHOD solve2.

    DATA(input) = split_string_to_lines( get_puzzle_input( part ) ).

    DATA: group_rucksack LIKE input.
    DATA(groups) = lines( input ) / 3.

    DO groups TIMES.

      DATA(start) = 1 + ( ( sy-index - 1 ) * 3 ).
      CLEAR group_rucksack.
      LOOP AT input ASSIGNING FIELD-SYMBOL(<line>) FROM start TO start + 2.
        INSERT <line> INTO TABLE group_rucksack.
      ENDLOOP.

      DATA(badge) = find_badge( group_rucksack ).
      DATA(priority) = prioritize( badge ).

      result = result + priority.

    ENDDO.

  ENDMETHOD.


  METHOD find_badge.

    TYPES:
      BEGIN OF t_letter_count,
        letter  TYPE t_char,
        count_1 TYPE abap_bool,
        count_2 TYPE abap_bool,
        count_3 TYPE abap_bool,
      END OF t_letter_count,
      tt_letter_count TYPE HASHED TABLE OF t_letter_count
                           WITH UNIQUE KEY letter
                           WITH NON-UNIQUE SORTED KEY count_key COMPONENTS count_1 count_2 count_3.

    DATA: letter_count TYPE tt_letter_count.

    LOOP AT split_string_to_chars( priorities ) ASSIGNING FIELD-SYMBOL(<letter>).
      INSERT VALUE #( letter = <letter> ) INTO TABLE letter_count.
    ENDLOOP.

    LOOP AT i_group_rucksack ASSIGNING FIELD-SYMBOL(<line>).

      DATA(index) = sy-tabix.

      LOOP AT split_string_to_chars( <line> ) ASSIGNING FIELD-SYMBOL(<char>).

        ASSIGN letter_count[ letter = <char> ] TO FIELD-SYMBOL(<letter_count>).
        ASSERT sy-subrc = 0.

        ASSIGN
          COMPONENT |COUNT_{ index }|
          OF STRUCTURE <letter_count>
          TO FIELD-SYMBOL(<count_flag>).
        ASSERT sy-subrc = 0.

        <count_flag> = abap_true.

      ENDLOOP.

    ENDLOOP.

    result = VALUE #( letter_count[ KEY count_key
                        count_1 = abap_true
                        count_2 = abap_true
                        count_3 = abap_true ]-letter ).

  ENDMETHOD.

ENDCLASS.
