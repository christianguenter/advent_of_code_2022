CLASS zcl_aoc_2022_04 DEFINITION
  PUBLIC
  INHERITING FROM zcl_aoc_2022_base
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      solve REDEFINITION,

      solve2 REDEFINITION,

      if_oo_adt_classrun~main REDEFINITION,

      constructor.

  PRIVATE SECTION.
    TYPES:
      BEGIN OF t_range,
        lower TYPE i,
        upper TYPE i,
      END OF t_range,
      BEGIN OF t_pair,
        first  TYPE t_range,
        second TYPE t_range,
      END OF t_pair,
      tt_pairs TYPE STANDARD TABLE OF t_pair WITH EMPTY KEY.

    METHODS:
      split_to_pairs
        IMPORTING
          input         TYPE string
        RETURNING
          VALUE(result) TYPE tt_pairs,

      split_to_range
        IMPORTING
          input         TYPE string
        RETURNING
          VALUE(result) TYPE t_range,

      is_pair_fully_contained
        IMPORTING
          i_pair        TYPE t_pair
        RETURNING
          VALUE(result) TYPE i,

      is_pair_partially_contained
        IMPORTING
          i_pair        TYPE t_pair
        RETURNING
          VALUE(result) TYPE i.
ENDCLASS.



CLASS zcl_aoc_2022_04 IMPLEMENTATION.

  METHOD constructor.

    super->constructor( day = 4 ).

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.

    me->out = out.

    ASSERT solve( c_puzzle-test ) = 2.
    ASSERT solve( c_puzzle-part_1 ) = 453.
    ASSERT solve2( c_puzzle-test ) = 4.
    ASSERT solve2( c_puzzle-part_2 ) = 919.

  ENDMETHOD.


  METHOD solve.

    DATA(pairs) = split_to_pairs( get_puzzle_input( part ) ).

    LOOP AT pairs ASSIGNING FIELD-SYMBOL(<pair>).

      result = result + is_pair_fully_contained( <pair> ).

    ENDLOOP.

  ENDMETHOD.


  METHOD solve2.

    DATA(pairs) = split_to_pairs( get_puzzle_input( part ) ).

    LOOP AT pairs ASSIGNING FIELD-SYMBOL(<pair>).

      result = result + is_pair_partially_contained( <pair> ).

    ENDLOOP.

  ENDMETHOD.


  METHOD split_to_pairs.

    LOOP AT split_string_to_lines( input ) ASSIGNING FIELD-SYMBOL(<line>).

      SPLIT <line> AT ',' INTO DATA(elve_one) DATA(elve_two).

      INSERT
        VALUE t_pair(
          first  = split_to_range( elve_one )
          second = split_to_range( elve_two ) )
        INTO TABLE result.

    ENDLOOP.

  ENDMETHOD.


  METHOD split_to_range.

    SPLIT input AT '-' INTO DATA(range_lower) DATA(range_upper).
    result-lower = range_lower.
    result-upper = range_upper.

  ENDMETHOD.


  METHOD is_pair_fully_contained.

    IF  ( i_pair-first-lower <= i_pair-second-lower
      AND i_pair-first-upper >= i_pair-second-upper )
    OR  ( i_pair-first-lower >= i_pair-second-lower
      AND i_pair-first-upper <= i_pair-second-upper ).
      result = 1.
    ENDIF.

  ENDMETHOD.


  METHOD is_pair_partially_contained.

    IF  ( i_pair-first-lower  <= i_pair-second-lower
      AND i_pair-first-upper  >= i_pair-second-lower )
    OR  ( i_pair-first-upper  >= i_pair-second-lower
      AND i_pair-first-upper  <= i_pair-second-upper )
    OR  ( i_pair-second-lower <= i_pair-first-lower
      AND i_pair-second-upper >= i_pair-first-lower )
    OR  ( i_pair-second-upper >= i_pair-first-lower
      AND i_pair-second-upper <= i_pair-first-upper ).
      result = 1.
    ENDIF.

  ENDMETHOD.

ENDCLASS.
