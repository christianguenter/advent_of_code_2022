CLASS zcl_aoc_2022_08 DEFINITION
  PUBLIC
  INHERITING FROM zcl_aoc_2022_base
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      constructor,
      solve REDEFINITION,
      solve2 REDEFINITION,
      if_oo_adt_classrun~main REDEFINITION.
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS zcl_aoc_2022_08 IMPLEMENTATION.

  METHOD constructor.

    super->constructor( day = 8 ).

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.

    me->out = out.
    ASSERT solve( c_puzzle-test ) = 21.
    ASSERT solve( c_puzzle-part_1 ) = 1708.
    ASSERT solve2( c_puzzle-test ) = 8.
    out->write( solve2( c_puzzle-part_2 ) ).

  ENDMETHOD.


  METHOD solve.

    TYPES:
      BEGIN OF t_visible,
        visible TYPE abap_bool,
      END OF t_visible.


    DATA(lines) = split_string_to_lines( get_puzzle_input( part ) ).

    LOOP AT lines ASSIGNING FIELD-SYMBOL(<line>).

      DATA(row_index) = sy-tabix.

      LOOP AT split_string_to_chars( <line> ) ASSIGNING FIELD-SYMBOL(<c>).

        DATA(col_index) = sy-tabix.

        DATA(is_visible) = xsdbool(
                             " visible from left?
                             REDUCE abap_bool(
                               INIT r = abap_true
                               FOR col = 1 WHILE col < col_index
                               NEXT r = COND #(
                                          WHEN r = abap_false THEN r
                                          WHEN col_index = 1 OR col_index = strlen( <line> ) THEN r
                                          WHEN substring(
                                                 val = lines[ row_index ]
                                                 off = col - 1
                                                 len = 1 ) >= <c>
                                          THEN abap_false
                                          ELSE abap_true ) ) = abap_true

                           OR

                            "visible from top?
                             REDUCE abap_bool(
                               INIT r = abap_true
                               FOR row = 1 WHILE row < row_index
                               NEXT r = COND #(
                                          WHEN r = abap_false THEN r
                                          WHEN row_index = 1 OR row_index = lines( lines ) THEN r
                                          WHEN substring(
                                                 val = lines[ row ]
                                                 off = col_index - 1
                                                 len = 1 ) >= <c>
                                          THEN abap_false
                                          ELSE abap_true ) ) = abap_true

                           OR

                             " visible from right?
                             REDUCE abap_bool(
                               INIT r = abap_true
                               FOR col = col_index WHILE col < strlen( <line> )
                               NEXT r = COND #(
                                          WHEN r = abap_false THEN r
                                          WHEN col_index = 1 OR col_index = strlen( <line> ) THEN r
                                          WHEN substring(
                                                 val = lines[ row_index ]
                                                 off = col
                                                 len = 1 ) >= <c>
                                          THEN abap_false
                                          ELSE abap_true ) ) = abap_true

                          OR
                             "visible from bottom?
                             REDUCE abap_bool(
                               INIT r = abap_true
                               FOR row = row_index + 1 WHILE row <= lines( lines )
                               NEXT r = COND #(
                                          WHEN r = abap_false THEN r
                                          WHEN row_index = 1 OR row_index = lines( lines ) THEN r
                                          WHEN substring(
                                                 val = lines[ row ]
                                                 off = col_index - 1
                                                 len = 1 ) >= <c>
                                          THEN abap_false
                                          ELSE abap_true ) ) = abap_true


                                         ).

*        out->write( |col: { col_index } row: { row_index } val: { <c> } visible: { is_visible }| ).

        result = result + COND i( WHEN is_visible = abap_true THEN 1 ).

      ENDLOOP.

    ENDLOOP.

  ENDMETHOD.


  METHOD solve2.

    TYPES:
      BEGIN OF t_visible,
        visible TYPE abap_bool,
      END OF t_visible.


    DATA(lines) = split_string_to_lines( get_puzzle_input( part ) ).

    LOOP AT lines ASSIGNING FIELD-SYMBOL(<line>).

      DATA(row_index) = sy-tabix.

      LOOP AT split_string_to_chars( <line> ) ASSIGNING FIELD-SYMBOL(<c>).

        DATA(col_index) = sy-tabix.

        DATA(scenic_score) =
                             " visible to left?
                             REDUCE i(
                               INIT r = 0 done = abap_false
                               FOR col = col_index - 1 THEN col - 1 WHILE col > 0
                               LET size = substring(
                                            val = lines[ row_index ]
                                            off = col - 1
                                            len = 1 )
                                   smaller = xsdbool( size < <c> )
                               IN
                               NEXT
                                    r = r + COND #(
                                              WHEN done = abap_true THEN 0
                                              WHEN smaller = abap_true OR done = abap_false THEN 1
                                              ELSE 0 )
                                    done = xsdbool( smaller = abap_false OR done = abap_true ) )

                           *

                            "visible to top?
                             REDUCE i(
                               INIT r = 0 done = abap_false
                               FOR row = row_index - 1 THEN row - 1 WHILE row > 0

                               LET size2 = substring(
                                                 val = lines[ row ]
                                                 off = col_index - 1
                                                 len = 1 )
                                   smaller2 = xsdbool( size2 < <c> )
                               IN
                               NEXT
                                    r = r + COND #(
                                              WHEN done = abap_true THEN 0
                                              WHEN smaller2 = abap_true OR done = abap_false THEN 1
                                              ELSE 0 )
                                    done = xsdbool( smaller2 = abap_false OR done = abap_true ) )

                           *

                             " visible to right?
                             REDUCE i(
                               INIT r = 0 done = abap_false
                               FOR col = col_index WHILE col < strlen( <line> )
                               LET size3 = substring(
                                                 val = lines[ row_index ]
                                                 off = col
                                                 len = 1 )
                                   smaller3 = xsdbool( size3 < <c> )
                               IN
                               NEXT
                                    r = r + COND #(
                                              WHEN done = abap_true THEN 0
                                              WHEN smaller3 = abap_true OR done = abap_false THEN 1
                                              ELSE 0 )
                                    done = xsdbool( smaller3 = abap_false OR done = abap_true ) )

                          *
                             "visible to bottom?
                             REDUCE i(
                               INIT r = 0 done = abap_false
                               FOR row = row_index + 1 WHILE row <= lines( lines )
                               LET size4 = substring(
                                                 val = lines[ row ]
                                                 off = col_index - 1
                                                 len = 1 )
                                   smaller4 = xsdbool( size4 < <c> )
                               IN
                               NEXT
                                    r = r + COND #(
                                              WHEN done = abap_true THEN 0
                                              WHEN smaller4 = abap_true OR done = abap_false THEN 1
                                              ELSE 0 )
                                    done = xsdbool( smaller4 = abap_false OR done = abap_true ) ).

*        out->write( |col: { col_index } row: { row_index } val: { <c> } scenic_score: { scenic_score }| ).

        result = nmax( val1 = result val2 = scenic_score ).

      ENDLOOP.

    ENDLOOP.

  ENDMETHOD.

ENDCLASS.
