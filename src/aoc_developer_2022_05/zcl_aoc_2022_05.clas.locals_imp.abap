*"* use this source file for the definition and implementation of
*"* local helper classes, interface definitions and type
*"* declarations

CLASS lcl_stack IMPLEMENTATION.

  METHOD if_stack~get_top.
    result = VALUE #( stack[ 1 ] OPTIONAL ).
  ENDMETHOD.

  METHOD if_stack~pop.
    result = if_stack~get_top( ).
    DELETE stack INDEX 1.
  ENDMETHOD.

  METHOD if_stack~push.
    INSERT elem INTO stack INDEX 1.
  ENDMETHOD.

  METHOD if_stack~print.

    LOOP AT stack ASSIGNING FIELD-SYMBOL(<s>).
      result = <s> && result.
    ENDLOOP.

  ENDMETHOD.

  METHOD if_stack~pop_n.

    DO count TIMES.
      INSERT if_stack~pop( ) INTO TABLE result.
    ENDDO.

  ENDMETHOD.

  METHOD if_stack~push_n.
    INSERT LINES OF cranes INTO stack INDEX 1.
  ENDMETHOD.

ENDCLASS.
