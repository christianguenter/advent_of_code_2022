*"* use this source file for any type of declarations (class
*"* definitions, interfaces or type declarations) you need for
*"* components in the private section

INTERFACE if_stack.
  TYPES:
    t_elem   TYPE c LENGTH 1,
    tt_elements TYPE STANDARD TABLE OF t_elem
                   WITH NON-UNIQUE DEFAULT KEY.

  METHODS:
    print
      RETURNING VALUE(result) TYPE string,

    push
      IMPORTING
        elem TYPE t_elem,

    push_n
      IMPORTING
        cranes TYPE tt_elements,

    pop
      RETURNING
        VALUE(result) TYPE t_elem,

    pop_n
      IMPORTING
        count         TYPE i
      RETURNING
        VALUE(result) TYPE tt_elements,

    get_top
      RETURNING
        VALUE(result) TYPE t_elem.

ENDINTERFACE.

CLASS lcl_stack DEFINITION.

  PUBLIC SECTION.
    INTERFACES:
      if_stack.

  PRIVATE SECTION.
    DATA: stack TYPE stringtab.

ENDCLASS.
