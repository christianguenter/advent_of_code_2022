CLASS zcl_aoc_2022_05 DEFINITION
  PUBLIC
  INHERITING FROM zcl_aoc_2022_base
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      constructor,
      solve REDEFINITION,
      solve2 REDEFINITION,
      if_oo_adt_classrun~main REDEFINITION.

  PROTECTED SECTION.
  PRIVATE SECTION.
    TYPES:
      BEGIN OF t_procedure,
        count TYPE i,
        from  TYPE i,
        to    TYPE i,
      END OF t_procedure,
      tt_stacks TYPE STANDARD TABLE OF REF TO if_stack WITH NON-UNIQUE DEFAULT KEY.

    DATA:
      stacks TYPE tt_stacks.

    METHODS:
      extract_lines
        IMPORTING
          input                    TYPE stringtab
        EXPORTING
          starting_stacks          TYPE stringtab
          rearrangement_procedures TYPE stringtab
          stacks_counter_line      TYPE string,

      build_initial_stacks
        IMPORTING
          starting_stacks     TYPE stringtab
          stacks_counter_line TYPE string,
      split_line_to_procedure
        IMPORTING
          input         TYPE string
        RETURNING
          VALUE(result) TYPE t_procedure,
      print_stack.

ENDCLASS.



CLASS zcl_aoc_2022_05 IMPLEMENTATION.


  METHOD build_initial_stacks.

    DATA(stacks_counter_tab) = split_string_to_chars( stacks_counter_line ).
    DELETE stacks_counter_tab WHERE table_line = space.
    DATA(stacks_counter) = stacks_counter_tab[ lines( stacks_counter_tab ) ].

    DO stacks_counter TIMES.
      INSERT CAST if_stack( NEW lcl_stack( ) ) INTO TABLE stacks.
    ENDDO.

    DATA: reversed_starting_stack LIKE starting_stacks.

    reverse_table(
      EXPORTING
        input  = starting_stacks
      IMPORTING
        result = reversed_starting_stack ).

    LOOP AT reversed_starting_stack ASSIGNING FIELD-SYMBOL(<line>).

      WHILE <line> IS NOT INITIAL.

        DATA(index) = sy-index.

        FIND FIRST OCCURRENCE OF REGEX '(\s\s\s|\[[A-Z]\])\s?' IN <line> MATCH OFFSET DATA(offset) MATCH LENGTH DATA(length).
        IF sy-subrc <> 0.
          EXIT.
        ENDIF.

        ASSIGN stacks[ index ] TO FIELD-SYMBOL(<stack>).
        ASSERT sy-subrc = 0.

        DATA(crane) = condense( substring(
                        val = <line>
                        off = offset + 1
                        len = length - 1 ) ).

        IF crane IS NOT INITIAL.
          <stack>->push( CONV #( crane ) ).
        ENDIF.

        <line> = substring(
                   val = <line>
                   off = offset + length ).

      ENDWHILE.

    ENDLOOP.

  ENDMETHOD.


  METHOD constructor.

    super->constructor( day = 5 ).

  ENDMETHOD.


  METHOD extract_lines.

    READ TABLE input TRANSPORTING NO FIELDS WITH KEY table_line = space.
    DATA(split_line) = sy-tabix.

    LOOP AT input ASSIGNING FIELD-SYMBOL(<line>) FROM 1 TO split_line - 2.
      INSERT <line> INTO TABLE starting_stacks.
    ENDLOOP.

    LOOP AT input ASSIGNING <line> FROM split_line + 1.
      INSERT <line> INTO TABLE rearrangement_procedures.
    ENDLOOP.

    READ TABLE input INTO stacks_counter_line INDEX split_line - 1.
    ASSERT sy-subrc = 0.

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.

    me->out = out.

    ASSERT solve( c_puzzle-test ) = 'CMZ'.
    CLEAR: stacks.

    ASSERT solve( c_puzzle-part_1 ) = 'TWSGQHNHL'.
    CLEAR: stacks.

    ASSERT solve2( c_puzzle-test ) = 'MCD'.
    CLEAR stacks.

    ASSERT solve2( c_puzzle-part_2 ) = 'JNRSCDWPP'.

  ENDMETHOD.


  METHOD print_stack.

    LOOP AT stacks ASSIGNING FIELD-SYMBOL(<stack>).
      out->write( |{ sy-tabix }: { <stack>->print( ) }| ).
    ENDLOOP.

  ENDMETHOD.


  METHOD solve.

    DATA(input) = split_string_to_lines( get_puzzle_input( part ) ).

    extract_lines(
      EXPORTING
        input                    = input
      IMPORTING
        starting_stacks          = DATA(starting_stacks)
        rearrangement_procedures = DATA(rearrangement_procedures)
        stacks_counter_line      = DATA(stacks_counter_line) ).

    build_initial_stacks(
        starting_stacks     = starting_stacks
        stacks_counter_line = stacks_counter_line ).

    LOOP AT rearrangement_procedures ASSIGNING FIELD-SYMBOL(<rearrangement_procedure>).

      DATA(procedure) = split_line_to_procedure( <rearrangement_procedure> ).

      DO procedure-count TIMES.
        stacks[ procedure-to ]->push( stacks[ procedure-from ]->pop( ) ).
      ENDDO.

    ENDLOOP.

    LOOP AT stacks ASSIGNING FIELD-SYMBOL(<stack>).
      result = result && <stack>->get_top( ).
    ENDLOOP.

  ENDMETHOD.


  METHOD solve2.

    DATA(input) = split_string_to_lines( get_puzzle_input( part ) ).

    extract_lines(
      EXPORTING
        input                    = input
      IMPORTING
        starting_stacks          = DATA(starting_stacks)
        rearrangement_procedures = DATA(rearrangement_procedures)
        stacks_counter_line      = DATA(stacks_counter_line) ).

    build_initial_stacks(
        starting_stacks     = starting_stacks
        stacks_counter_line = stacks_counter_line ).

    LOOP AT rearrangement_procedures ASSIGNING FIELD-SYMBOL(<rearrangement_procedure>).

      DATA(procedure) = split_line_to_procedure( <rearrangement_procedure> ).

      stacks[ procedure-to ]->push_n( stacks[ procedure-from ]->pop_n( procedure-count ) ).

    ENDLOOP.

    LOOP AT stacks ASSIGNING FIELD-SYMBOL(<stack>).
      result = result && <stack>->get_top( ).
    ENDLOOP.

  ENDMETHOD.


  METHOD split_line_to_procedure.

    FIND
      FIRST OCCURRENCE OF REGEX 'move\s(\d+)\sfrom\s(\d+)\sto\s(\d+)'
      IN input SUBMATCHES DATA(count) DATA(from) DATA(to).

    result-count = count.
    result-from = from.
    result-to = to.

  ENDMETHOD.
ENDCLASS.
