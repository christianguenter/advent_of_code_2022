*"* use this source file for any type of declarations (class
*"* definitions, interfaces or type declarations) you need for
*"* components in the private section
CLASS lcl_rope DEFINITION.

  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING
          i_knots TYPE i,

      move
        IMPORTING
          direction TYPE string
          steps     TYPE i,

      print
        RETURNING
          VALUE(result) TYPE string,

      get_tail_positions
        RETURNING
          VALUE(result) TYPE i,

      print_tail_pos
        IMPORTING
          out TYPE REF TO if_oo_adt_intrnl_classrun.

  PRIVATE SECTION.
    TYPES:
      BEGIN OF t_coord,
        x TYPE i,
        y TYPE i,
      END OF t_coord.

    DATA:
      knots      TYPE STANDARD TABLE OF t_coord
                      WITH NON-UNIQUE DEFAULT KEY,

      start      TYPE t_coord,

      head       TYPE REF TO t_coord,
      tail       TYPE REF TO t_coord,

      tail_track TYPE HASHED TABLE OF t_coord
                      WITH UNIQUE KEY x y.

    METHODS:
      move_tail_if_needed,

      is_tail_move_needed
        IMPORTING
          i_knot1       TYPE REF TO t_coord
          i_knot2       TYPE REF TO t_coord
        RETURNING
          VALUE(result) TYPE abap_bool,

      move_tail
        IMPORTING
          i_knot1       TYPE REF TO t_coord
          i_knot2       TYPE REF TO t_coord,

      invariant.


ENDCLASS.
