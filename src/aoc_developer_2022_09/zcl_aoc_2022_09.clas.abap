CLASS zcl_aoc_2022_09 DEFINITION
  PUBLIC
  INHERITING FROM zcl_aoc_2022_base
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      constructor,
      solve REDEFINITION,
      solve2 REDEFINITION,
      if_oo_adt_classrun~main REDEFINITION.

ENDCLASS.



CLASS zcl_aoc_2022_09 IMPLEMENTATION.

  METHOD constructor.

    super->constructor( day = 9 ).

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.

    me->out = out.
    ASSERT solve( c_puzzle-test ) = 13.
    ASSERT solve( c_puzzle-part_1 ) = 6271.
    ASSERT solve2( c_puzzle-test ) = 1.
    ASSERT solve2( c_puzzle-part_2 ) = 36.
    ASSERT solve2( c_puzzle-part_1 ) = 2458.

  ENDMETHOD.


  METHOD solve.

    DATA(rope) = NEW lcl_rope( 2  ).

    LOOP AT split_string_to_lines( get_puzzle_input( part ) ) ASSIGNING FIELD-SYMBOL(<line>).

      FIND FIRST OCCURRENCE OF REGEX '([A-Z])\s([0-9]+)'  IN <line>
        SUBMATCHES DATA(direction) DATA(steps).

      rope->move(
        direction = direction
        steps     = CONV #( steps ) ).

    ENDLOOP.

    result = rope->get_tail_positions( ).

  ENDMETHOD.


  METHOD solve2.

    DATA(rope) = NEW lcl_rope( 10 ).

    LOOP AT split_string_to_lines( get_puzzle_input( part ) ) ASSIGNING FIELD-SYMBOL(<line>).

      FIND FIRST OCCURRENCE OF REGEX '([A-Z])\s([0-9]+)'  IN <line>
        SUBMATCHES DATA(direction) DATA(steps).

*      out->write( |move { direction } { steps }| ).

      rope->move(
        direction = direction
        steps     = CONV #( steps ) ).

*      out->write( rope->print( ) ).

    ENDLOOP.

*    rope->print_tail_pos( out ).

    result = rope->get_tail_positions( ).

  ENDMETHOD.

ENDCLASS.
