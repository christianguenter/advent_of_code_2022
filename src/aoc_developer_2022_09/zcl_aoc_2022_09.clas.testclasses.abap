*"* use this source file for your ABAP unit test classes
CLASS ltcl_test DEFINITION FINAL FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS.

  PRIVATE SECTION.
    DATA cut TYPE REF TO zcl_aoc_2022_09.
    METHODS:
      setup,

      solve FOR TESTING RAISING cx_static_check,
      solve2 FOR TESTING RAISING cx_static_check.

ENDCLASS.


CLASS ltcl_test IMPLEMENTATION.

  METHOD setup.

    cut = NEW zcl_aoc_2022_09( ).

  ENDMETHOD.


  METHOD solve.

    cl_abap_unit_assert=>assert_equals(
        exp = cut->solve( cut->c_puzzle-test )
        act = 13 ).

    cl_abap_unit_assert=>assert_equals(
        exp = cut->solve( cut->c_puzzle-part_1 )
        act = 6271 ).

  ENDMETHOD.


  METHOD solve2.

    cl_abap_unit_assert=>assert_equals(
        exp = cut->solve2( cut->c_puzzle-test )
        act = 1 ).

    cl_abap_unit_assert=>assert_equals(
        exp = cut->solve2( cut->c_puzzle-part_2 )
        act = 36 ).

    cl_abap_unit_assert=>assert_equals(
        exp = cut->solve2( cut->c_puzzle-part_1 )
        act = 2458 ).

  ENDMETHOD.

ENDCLASS.
