*"* use this source file for the definition and implementation of
*"* local helper classes, interface definitions and type
*"* declarations

CLASS lcl_rope IMPLEMENTATION.

  METHOD constructor.

    start = VALUE t_coord( x = 0 y = 0 ).

    DO i_knots TIMES.
      INSERT start INTO TABLE knots.
    ENDDO.

    head = REF #( knots[ 1 ] ).
    tail = REF #( knots[ lines( knots ) ] ).

    INSERT tail->* INTO TABLE tail_track.

    invariant( ).

  ENDMETHOD.


  METHOD move.

    DO steps TIMES.
      CASE direction.
        WHEN 'R'.

          head->x = head->x + 1.
          move_tail_if_needed( ).

        WHEN 'U'.

          head->y = head->y + 1.
          move_tail_if_needed( ).

        WHEN 'D'.

          head->y = head->y - 1.
          move_tail_if_needed( ).

        WHEN 'L'.

          head->x = head->x - 1.
          move_tail_if_needed( ).

      ENDCASE.

      invariant( ).

    ENDDO.

  ENDMETHOD.


  METHOD move_tail_if_needed.

    DATA: prev TYPE REF TO t_coord.

    LOOP AT knots REFERENCE INTO DATA(knot) FROM 1.

      IF prev IS NOT BOUND.
        prev = knot.
        CONTINUE.
      ENDIF.

      IF is_tail_move_needed(
             i_knot1 = prev
             i_knot2 = knot ) = abap_false.
        RETURN.
      ENDIF.

      DATA(old_knot) = knot->*.

      move_tail(
           i_knot1 = prev
           i_knot2 = knot ).
      INSERT tail->* INTO TABLE tail_track.

      IF abs( abs( old_knot-x ) - abs( knot->x ) ) > 1.
        BREAK developer.
        ASSERT 1 = 2.
      ENDIF.

      IF abs( abs( old_knot-y ) - abs( knot->y ) ) > 1.
        BREAK developer.
        ASSERT 1 = 2.
      ENDIF.

      prev = knot.

    ENDLOOP.

  ENDMETHOD.


  METHOD is_tail_move_needed.

    IF i_knot1->x > i_knot2->x.
      result = boolc( i_knot1->x - i_knot2->x > 1 ).
    ELSE.
      result = boolc( i_knot2->x - i_knot1->x > 1 ).
    ENDIF.

    IF result = abap_true.
      RETURN.
    ENDIF.

    IF i_knot1->y > i_knot2->y.
      result = boolc( i_knot1->y - i_knot2->y > 1 ).
    ELSE.
      result = boolc( i_knot2->y - i_knot1->y > 1 ).
    ENDIF.

  ENDMETHOD.


  METHOD move_tail.

    IF i_knot1->x = i_knot2->x.
      IF i_knot2->y > i_knot1->y.
        i_knot2->y = i_knot1->y + 1.
      ELSE.
        i_knot2->y = i_knot1->y - 1.
      ENDIF.
      RETURN.
    ENDIF.

    IF i_knot1->y = i_knot2->y.
      IF i_knot2->x > i_knot1->x.
        i_knot2->x = i_knot1->x + 1.
      ELSE.
        i_knot2->x = i_knot1->x - 1.
      ENDIF.
      RETURN.
    ENDIF.

    " diagonal

    IF  abs( i_knot1->x - i_knot2->x ) > 1
    AND abs( i_knot1->y - i_knot2->y ) > 1.

      IF i_knot2->x > i_knot1->x.
        i_knot2->x = i_knot1->x + 1.
      ELSE.
        i_knot2->x = i_knot1->x - 1.
      ENDIF.

      IF i_knot2->y > i_knot1->y.
        i_knot2->y = i_knot1->y + 1.
      ELSE.
        i_knot2->y = i_knot1->y - 1.
      ENDIF.

      RETURN.
    ENDIF.

    IF abs( i_knot1->x - i_knot2->x ) > 1.
      IF i_knot2->x > i_knot1->x.
        i_knot2->x = i_knot1->x + 1.
      ELSE.
        i_knot2->x = i_knot1->x - 1.
      ENDIF.
      i_knot2->y = i_knot1->y.
      RETURN.
    ENDIF.

    IF abs( i_knot1->y - i_knot2->y ) > 1.
      IF i_knot2->y > i_knot1->y.
        i_knot2->y = i_knot1->y + 1.
      ELSE.
        i_knot2->y = i_knot1->y - 1.
      ENDIF.
      i_knot2->x = i_knot1->x.
      RETURN.
    ENDIF.

    ASSERT 1 = 2.

  ENDMETHOD.


  METHOD print.

    result = |Head: x={ head->x } y={ head->y }\tTail: x={ tail->x } y={ tail->y }|.

  ENDMETHOD.


  METHOD get_tail_positions.

    result = lines( tail_track ).

  ENDMETHOD.


  METHOD invariant.

    DATA: prev TYPE REF TO t_coord.

    LOOP AT knots REFERENCE INTO DATA(knot).

      IF prev IS NOT BOUND.
        prev = knot.
        CONTINUE.
      ELSE.
        ASSERT is_tail_move_needed(
                 i_knot1 = prev
                 i_knot2 = knot ) = abap_false.
        prev = knot.
      ENDIF.

    ENDLOOP.


  ENDMETHOD.


  METHOD print_tail_pos.

    out->write( tail_track ).

  ENDMETHOD.

ENDCLASS.
